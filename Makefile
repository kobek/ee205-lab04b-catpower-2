###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05b - integers - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build and test a program that explores integers
###
### @author  Kobe Uyeda <kobek@hawaii.edu>
### @date    15_Feb_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

# Build a datatype explorer program

HOSTNAME=$(shell hostname --short)
$(info $(HOSTNAME))

CC     = g++
CFLAGS = -g -Wall -Wextra -DHOST=\"$(HOSTNAME)\"

TARGET = catPower

all: clearscreen $(TARGET)

.PHONY: clearscreen

clearscreen:
	clear

# For now, do not use Make's % expansion parameters.  Each file must have its own 
# build instructions.  We will use the % expansion later.
ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp

cat.o: cat.cpp cat.h
	$(CC) $(CFLAGS) -c cat.cpp

foe.o: foe.cpp foe.h
	$(CC) $(CFLAGS) -c foe.cpp

gge.o: gge.cpp gge.h
	$(CC) $(CFLAGS) -c gge.cpp

megaton.o: megaton.cpp megaton.h
	$(CC) $(CFLAGS) -c megaton.cpp

catPower.o: catPower.cpp ev.h cat.h foe.h gge.h megaton.h joule.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o ev.o cat.o foe.o gge.o megaton.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o ev.o cat.o foe.o gge.o megaton.o

test: clearscreen $(TARGET)
	@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e | grep -q "3.14 j is 1.95983E+19 e"
	@./catPower 3.14 j m | grep -q "3.14 j is 7.50478E-16 m"
	@./catPower 3.14 j g | grep -q "3.14 j is 2.58862E-08 g"
	@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-24 f"
	@./catPower 3.14 j c | grep -q "3.14 j is 0 c"
	@./catPower 2 g c | grep -q "2 g is 0 c"
	@./catPower 2 g f | grep -q "2 g is 2.426E-16 f"
	@./catPower 2 g j | grep -q "2 g is 2.426E+08 j"
	@./catPower 2 g g | grep -q "2 g is 2 g"
	@./catPower 2 g m | grep -q "2 g is 5.79828E-08 m"
	@./catPower 2 g e | grep -q "2 g is 1.51419E+27 e"
	@./catPower 2 x j | grep -q "Unknown fromUnit \[x\]"
	@./catPower 2 x j | grep -q "Usage:  catPower fromValue fromUnit toUnit"
	@./catPower 3.14 j x |& grep -q "Unknown toUnit \[x\]"
	
	@echo "All tests pass"


clean:
	rm -f *.o $(TARGET)

