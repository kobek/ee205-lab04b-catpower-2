///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Kobe Uyeda <kobek@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//

const double GASOLINE_GALLON_EQUIVALENT_IN_A_JOULE = 1 / 1.213e8;
const char GASOLINE_GALLON_EQUIVALENT              = 'g';

extern double fromGasolineGallonEquivalentToJoule( double gassolineGallon );
extern double fromJouleToGasolineGallonEquivalent( double joule );
